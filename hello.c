///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01 - Hello World
//
// @author Albert D'Sanson <adsanson@hawaii.edu>
// @date   11_01_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello World!\n");
   return 0;
}
